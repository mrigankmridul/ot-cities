import { render, screen } from "@testing-library/react";
import Footer from ".";

describe("Footer", () => {
  it("renders the footer component correctly", () => {
    render(<Footer />);
    screen.getByText("Insurances@Ottonova");
    screen.getByText("©2022 Ottonova.dev");
  });
});

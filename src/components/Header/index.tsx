import React, { useState } from "react";
import {
  createStyles,
  Header as Headers,
  Container,
  Group,
} from "@mantine/core";
import { useRouter } from "next/router";

const links = [
  {
    link: "/",
    label: "Home",
  },
  {
    link: "/cities",
    label: "Cities",
  },
  {
    link: "/api/cities",
    label: "API Cities",
  },
];

const useStyles = createStyles((theme) => ({
  header: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    height: "100%",
  },

  link: {
    display: "block",
    lineHeight: 1,
    padding: "8px 12px",
    borderRadius: theme.radius.sm,
    textDecoration: "none",
    color:
      theme.colorScheme === "dark"
        ? theme.colors.dark[0]
        : theme.colors.gray[7],
    fontSize: theme.fontSizes.sm,
    fontWeight: 500,

    "&:hover": {
      backgroundColor:
        theme.colorScheme === "dark"
          ? theme.colors.dark[6]
          : theme.colors.gray[0],
    },
  },

  linkActive: {
    "&, &:hover": {
      backgroundColor:
        theme.colorScheme === "dark"
          ? theme.fn.rgba(theme.colors[theme.primaryColor][9], 0.25)
          : theme.colors[theme.primaryColor][0],
      color:
        theme.colors[theme.primaryColor][theme.colorScheme === "dark" ? 3 : 7],
    },
  },
}));

export default function Header() {
  const router = useRouter();

  const [active, setActive] = useState(
    links.find((link) => link.link === router.pathname).link
  );
  const { classes, cx } = useStyles();

  const items = links.map((link) => (
    <a
      key={link.label}
      href={link.link}
      className={cx(classes.link, {
        [classes.linkActive]: active === link.link,
      })}
      onClick={(event) => {
        event.preventDefault();
        setActive(link.link);
        router.push(link.link);
      }}
    >
      {link.label}
    </a>
  ));

  return (
    <Headers height={60} mb={10}>
      <Container className={classes.header}>
        Ottonova Logo
        <Group spacing={5}>{items}</Group>
      </Container>
    </Headers>
  );
}

import { render, screen } from "@testing-library/react";
import { useRouter } from "next/router";

import Header from ".";

jest.mock("next/router", () => ({
  useRouter: jest.fn(),
}));

describe("Header", () => {
  (useRouter as jest.Mock).mockImplementation(() => ({
    push: jest.fn(),
    pathname: "/",
  }));
  it("renders the header component correctly", () => {
    render(<Header />);
    screen.getByText("Ottonova Logo");
  });
});

import { render, screen } from "@testing-library/react";
import CityComponent from ".";
export const cityMockProps = {
  name: "Sydney",
  founded: "1788",
  population: "5312000",
  nativeName: "Sydney",
  location: {
    country: "Australia",
    continent: "Australia",
    latitude: "-33.865143",
    longitude: "151.209900",
  },
  landmarks: [
    "Sydney Opera House",
    "Sydney Harbour Bridge",
    "Queen Victoria Building",
  ],
};
describe("City", () => {
  it("renders a city card correctly", () => {
    render(<CityComponent {...cityMockProps} />);
    screen.getByText("Sydney Opera House");
    screen.getByText("Sydney Harbour Bridge");
    screen.getByText("Queen Victoria Building");
  });
});

import React from "react";
import {
  createStyles,
  Card,
  Image,
  Text,
  Group,
  Badge,
  useMantineTheme,
} from "@mantine/core";
import { City } from "../../interfaces";
import Link from "next/link";

const useStyles = createStyles((theme) => ({
  card: {
    backgroundColor:
      theme.colorScheme === "dark" ? theme.colors.dark[7] : theme.white,
    height: "100%",
  },

  title: {
    fontFamily: `Greycliff CF, ${theme.fontFamily}`,
    lineHeight: 1,
  },

  section: {
    paddingLeft: theme.spacing.md,
    paddingRight: theme.spacing.md,
    paddingBottom: theme.spacing.md,
  },

  label: {
    fontSize: theme.fontSizes.xs,
    fontWeight: 500,
  },
}));

export default function CityComponent({
  name,
  nativeName,
  location,
  population,
  landmarks,
}: City) {
  const { classes } = useStyles();
  const theme = useMantineTheme();

  return (
    <Card withBorder p="lg" className={classes.card}>
      <Card.Section>
        <Image
          src={`https://picsum.photos/seed/${name}/300`}
          alt={name}
          height={100}
        />
      </Card.Section>

      <Group position="apart" mt="xl">
        <Text size="sm" weight={700} className={classes.title}>
          {name} {name !== nativeName && `(${nativeName})`}
        </Text>
        <Group spacing={5}>
          <Link
            href={`https://maps.google.com/?q=${location.latitude},${location.longitude}`}
          >
            <a target="_blank">🗺️ Google Maps</a>
          </Link>
        </Group>
      </Group>
      <Text mb="md" color="dimmed" size="xs">
        {location.country}, {location.continent}
        <Text size="xs" mt={4} color="dimmed">
          👤 {new Intl.NumberFormat("en-DE").format(parseInt(population, 10))}
        </Text>
      </Text>
      <Card.Section className={classes.section}>
        <Text className={classes.label} color="dimmed">
          City Attractions
        </Text>
        <Group spacing={7} mt={4}>
          {landmarks.map((landmark) => (
            <Badge
              color={theme.colorScheme === "dark" ? "dark" : "gray"}
              key={landmark}
            >
              {landmark}
            </Badge>
          ))}
        </Group>
      </Card.Section>
    </Card>
  );
}

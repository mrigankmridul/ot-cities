import { render, screen } from "@testing-library/react";
import { useRouter } from "next/router";

import Layout from ".";

jest.mock("next/router", () => ({
  useRouter: jest.fn(),
}));

describe("Layout", () => {
  (useRouter as jest.Mock).mockImplementation(() => ({
    push: jest.fn(),
    pathname: "/",
  }));
  it("renders the layout component correctly", () => {
    render(<Layout>child text</Layout>);
    screen.getByText("child text");
  });
});

import React, { ReactNode } from "react";
import Footer from "../Footer";
import Header from "../Header";

type Props = {
  children?: ReactNode;
  title?: string;
};

export default function Layout({
  children,
  title = "Ottonova Insurances",
}: Props) {
  return (
    <div>
      <title>{title}</title>
      <Header />
      {children}
      <Footer />
    </div>
  );
}

import { render, screen } from "@testing-library/react";
import { cityMockProps } from "../City/index.test";
import Cities from ".";
export const citiesMockProps = {
  cities: [cityMockProps],
};
describe("Cities", () => {
  it("renders cities list correctly", () => {
    render(<Cities {...citiesMockProps} />);
    screen.getByText("Sydney Opera House");
    screen.getByText("Sydney Harbour Bridge");
    screen.getByText("Queen Victoria Building");
  });
});

import { Grid } from "@mantine/core";
import * as React from "react";
import { City } from "../../interfaces";
import CityComponent from "../City";

type Props = {
  cities: City[];
};

export default function CityList({ cities }: Props) {
  return (
    <Grid mt={40} mb={10}>
      {cities.map((item) => (
        <Grid.Col span={4} key={item.name}>
          <CityComponent {...item} />
        </Grid.Col>
      ))}
    </Grid>
  );
}

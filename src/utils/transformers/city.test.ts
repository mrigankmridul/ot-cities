import cityTransformer from "./city";

test("Transforms city properly", () => {
  expect(
    cityTransformer({
      name: "Sydney",
      name_native: "Sydney",
      country: "Australia",
      continent: "Australia",
      latitude: "-33.865143",
      longitude: "151.209900",
      population: "5312000",
      founded: "1788",
      landmarks: [
        "Sydney Opera House",
        "Sydney Harbour Bridge",
        "Queen Victoria Building",
      ],
    })
  ).toStrictEqual({
    name: "Sydney",
    founded: "1788",
    population: "5312000",
    nativeName: "Sydney",
    location: {
      country: "Australia",
      continent: "Australia",
      latitude: "-33.865143",
      longitude: "151.209900",
    },
    landmarks: [
      "Sydney Opera House",
      "Sydney Harbour Bridge",
      "Queen Victoria Building",
    ],
  });
});

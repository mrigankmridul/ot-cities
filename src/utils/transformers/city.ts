import { City, CityDB } from "../../interfaces";

const cityTransformer = (city: CityDB): City => ({
  name: city.name,
  founded: city.founded,
  population: city.population,
  nativeName: city.name_native,
  location: {
    country: city.country,
    continent: city.continent,
    latitude: city.latitude,
    longitude: city.longitude,
  },
  landmarks: city.landmarks,
});

export default cityTransformer;

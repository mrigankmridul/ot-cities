export type CityDB = {
  name: string;
  name_native: string;
  country: string;
  continent: string;
  latitude: string;
  longitude: string;
  population: string;
  founded: string;
  landmarks: string[];
};

export type City = {
  name: string;
  founded: string;
  population: string;
  nativeName: string;
  location: {
    country: string;
    continent: string;
    latitude: string;
    longitude: string;
  };
  landmarks: string[];
};

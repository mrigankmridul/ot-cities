import { Title, Text } from "@mantine/core";
import Link from "next/link";
import Layout from "../src/components/Layout";

const IndexPage = () => (
  <Layout>
    <Title align="center" mt={250} mb={250}>
      Hello Ottonova 👋
      <Text mt={5} align="center" size="md">
        <Link href="/cities">Click here</Link> to view cities
      </Text>
    </Title>
  </Layout>
);

export default IndexPage;

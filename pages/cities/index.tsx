import { GetStaticProps } from "next";
import Link from "next/link";

import { City } from "../../src/interfaces";
import Layout from "../../src/components/Layout";
import List from "../../src/components/CityList";
import axios from "../../src/utils/axios";
import { Container, Title } from "@mantine/core";

type Props = {
  cities: City[];
};

const WithStaticProps = ({ cities }: Props) => (
  <Layout>
    <Title align="center" mb={10}>
      City List
    </Title>
    <Container size="lg" px="md">
      <List cities={cities} />
    </Container>
  </Layout>
);

export async function getServerSideProps() {
  const response = await axios.get("api/cities");
  return {
    props: {
      cities: response.data,
    },
  };
}

export default WithStaticProps;

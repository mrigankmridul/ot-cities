import { NextApiRequest, NextApiResponse } from "next";
import logger from "../../../src/utils/logger";
import cityJson from "../../../src/utils/data/cities.json";
import cityTransformer from "../../../src/utils/transformers/city";

/**
 * @swagger
 * /api/cities:
 *   get:
 *     description: Returns cities list
 *     responses:
 *       200:
 *         cities: [
 *           {
 *             "name": "Hamburg",
 *             "name_native": "Hamburg",
 *             "country": "Germany",
 *             "continent": "Europe",
 *             "latitude": "-33.865143",
 *             "longitude": "151.209900",
 *             "population": "111111",
 *             "founded": "1947",
 *             "landmarks": [
 *               "Landmarks 11",
 *               "Landmarks 22",
 *               "Landmarks 33"
 *             ]
 *           },
 *         ]
 */
const handler = (req: NextApiRequest, res: NextApiResponse) => {
  try {
    logger.info(`${req.method} cities`);
    switch (req.method) {
      case "GET":
        const mappedCities = cityJson.cities.map((city) =>
          cityTransformer(city)
        );
        res.status(200).json(mappedCities);
      default:
        res.status(404).end();
    }
  } catch (err: any) {
    logger.error(`${req.method} cities`);
    res.status(500).json({ statusCode: 500, message: err.message });
  }
};

export default handler;

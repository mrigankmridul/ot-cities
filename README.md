# Ottonova Cities

### Built With

This project is powered by:

- [TypeScript](https://www.typescriptlang.org)
- [Next.js](https://nextjs.org/)
- [React.js](https://reactjs.org/)
- [Jest](https://jestjs.io/)
- [Cypress](https://docs.cypress.io/)

## Getting Started

### Prerequisites

Please make sure you have nodejs and npm installed in your machine.

### Dev server

To install dependencies, run:

```bash
yarn
```

To run the development server (including hot reloading and error reporting):

```bash
yarn dev
```

You can now access the dev environment on [http://localhost:3000](http://localhost:3000)

### Run Tests:

We use Jest for unit testing and cypress for e2e testing.
Run all tests:

```sh
    yarn test
```

Run Unit tests: (To measure unit test coverage run `yarn test:coverage`)

```sh
    yarn test:jest
```

End to End tests: (Please make sure the local dev server is running before running e2e tests)

```sh
   yarn test:cypress
```

Coverage:

| File                | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s |
| ------------------- | ------- | -------- | ------- | ------- | ----------------- |
| All files           | 92.59   | 52       | 88.23   | 92      |
| components/City     | 100     | 50       | 100     | 100     |
| index.tsx           | 100     | 50       | 100     | 100     | 17-83             |
| components/CityList | 100     | 100      | 100     | 100     |
| index.tsx           | 100     | 100      | 100     | 100     |
| components/Footer   | 92.3    | 50       | 80      | 91.66   |
| index.tsx           | 92.3    | 50       | 80      | 91.66   | 150               |
| components/Header   | 82.35   | 50       | 80      | 81.25   |
| index.tsx           | 82.35   | 50       | 80      | 81.25   | 82-84             |
| components/Layout   | 100     | 100      | 100     | 100     |
| index.tsx           | 100     | 100      | 100     | 100     |
| utils/transformers  | 100     | 100      | 100     | 100     |
| city.ts             | 100     | 100      | 100     | 100     |

### Build

To build the bundle use :

```sh
yarn build
```

### Project folder structure

    ├── cypress                  # All end to end test files
    ├── pages                    # NextJs client routes and api routes
    ├── src                      # Source files (alternatively `lib` or `app`)
    │   ├── components           #  UI components
    │   ├── interfaces           #  Types
    │   ├── utils                # Miscellaneous information
    |   |   ├── axios            #  Reuseale axios instance
    |   |   ├── data             # Persistant data for backend
    |   |   ├── logger           #  Logger function for backend
    |   |   ├── transformers     #  Transformers for api resolvers
    ├── *                        # Other project set up files

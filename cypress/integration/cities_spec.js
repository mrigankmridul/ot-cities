describe("Cities", () => {
  it("should load homepage", () => {
    cy.visit("http://localhost:3000/");
    cy.contains("Hello Ottonova");
  });

  it("should navigate to cities", () => {
    cy.visit("http://localhost:3000/");
    cy.contains("Cities").click();
    cy.url().should("include", "/cities");
  });

  it("should render ottonova office", () => {
    cy.visit("http://localhost:3000/cities");
    cy.contains("ottonova office");
  });
});
